﻿<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title>Máy tính</title>
	<link rel="stylesheet" href="{{ URL::asset('css/style.css') }}" type="text/css" />
</head>
<body>
    <div class="container">
		<div class="calc-body">
            <form name="Calc" action="{{ url('index') }}" method="POST">
                @csrf()
                <table>
                    <div class="calc-screen">
                        <div class="calc-operation">
                            <input class="number" placeholder="Nhập số ở đây" value="{{$a ?? ''}}" type="text" name="a" size="23">
                        </div>
                    </div>
                    <tr>
                        <td>
                            <select name="pheptinh">
                                <option>{{ $pheptinh ?? 'Phép Tính'}}</option>
                                <option value="+">+</option>
                                <option value="-">-</option>
                                <option value="x">x</option>
                                <option value="/">/</option>
                            </select>
                        </td>
                        <td><input class="button l" type="submit" value="=" /></td>
                    </tr>
                    <div class="calc-screen">
                            <div class="calc-operation">
                            <input class="number" placeholder="Nhập số ở đây" value="{{$b ?? ''}}" type="text" name="b" size="23">
                        </div>
                    </div>
                </table>
                    <div class="calc-screen" >
                        <div class="calc-operation" style="border: 0;">
                        <input class="number" placeholder="Nhập số ở đây" value="{{$ketqua ?? ''}}" type="text" size="23">
                    </div>
                </div>
            </form>
		</div>
    </div>
</body>
</html>
